
class PID(object):

    def __init__(self, k_p, k_i, k_d, t_step):
        self.k_p = k_p
        self.k_d = k_d
        self.k_i = k_i
        self.t_step = t_step
        self.previous_error = 0
        self.accumulated_error = 0
        return

    def step(self, reference, output):
        error = reference - output
        self.accumulated_error += error*self.t_step
        error_change = (error - self.previous_error)/self.t_step
        control_value = error*self.k_p + self.accumulated_error*self.k_i + error_change*self.k_d
        self.previous_error = error
        return control_value