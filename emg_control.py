
'''
Channel 1: Left Bicep
Channel 2: Left Tricep
Controls X

Channel 3: Right Bicep
Channel 4: Right TricepCotrols Y

Maak 2 channels die tussen -1 en 1 gaan en daar van een p_dot vector maken
'''
import ulab as np

class Converter(object):

    def __init__(self, bicep1, tricep1, bicep2, tricep2, sensor_state):
        self.bicep1 = bicep1
        self.tricep1 = tricep1
        self.bicep2 = bicep2
        self.tricep2 = tricep2
        self.sensor_state = sensor_state
        self.arm1 = 0
        self.arm2 = 0
        return

    def channel_diff(self):
        self.arm1 = self.bicep1.envelope(self.sensor_state.emg_values[0]) - 2*self.tricep1.envelope(self.sensor_state.emg_values[1])
        self.arm2 = self.bicep2.envelope(self.sensor_state.emg_values[2]) - 1*self.tricep2.envelope(self.sensor_state.emg_values[3])
        return

    def emg_to_velocity(self, v_max = 0.1):

        """Converts the filtered signal values to a desired velocity"""

        v_max = np.array([[v_max],[v_max]])
        self.channel_diff()
        input = np.array([[self.arm1],[self.arm2]])
        v_desired = input*v_max
        return v_desired
