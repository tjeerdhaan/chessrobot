
class Biquad(object):

    def __init__(self, a_coeff, b_coeff):
        """
        Requires 3 a-coefficients and 3 b-coefficients, 
        where the coefficients are normalized so that a0 = 1
        """
        self.a_coeff = a_coeff
        self.b_coeff = b_coeff
        self.delay_register = [0.0, 0.0]
        return

    def step(self, sample):
        """
        Implements biquad filter step using transposed direct form 2.
        """
        filtered = self.b_coeff[0]*sample + self.delay_register[0]
        self.delay_register[0] = self.delay_register[1] + self.b_coeff[1]*sample - self.a_coeff[1]*filtered
        self.delay_register[1] = self.b_coeff[2]*sample - self.a_coeff[2]*filtered
        return filtered

    