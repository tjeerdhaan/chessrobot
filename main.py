from state_machine import StateMachine
import micropython

micropython.alloc_emergency_exception_buf(100)
robot = StateMachine(250)
robot.start()
