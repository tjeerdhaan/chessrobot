from biorobotics import PWM
from pin_definitions import Pins
from machine import Pin

class Motor(object):

    def __init__(self, motor, pwm_frequency):

        if motor == 1:
            self.pwm = PWM(Pins.MOTOR_1_PWM, pwm_frequency)
            self.direction = Pin(Pins.MOTOR_1_DIRECTION, Pin.OUT)
            self.motor_id = 1
        else:
            self.pwm = PWM(Pins.MOTOR_2_PWM, pwm_frequency)
            self.direction = Pin(Pins.MOTOR_2_DIRECTION, Pin.OUT)
            self.motor_id = 2
        return

    def write(self, value):
        #negative value has a clockwise direction, positive value has anti-clockwise direction.
        if value < 0 and self.motor_id == 1:
            self.direction.value(0)
        elif self.motor_id == 1:
            self.direction.value(1)
        elif self.motor_id == 2 and value < 0:
            self.direction.value(1)
        else:
            self.direction.value(0)
        
        #Limit the motor pwm to a maximum of 0.7, to prevent any undesirably fast movements
        if abs(value) > 0.7:
            self.pwm.write(0.7)
        else:
            self.pwm.write(abs(value))
        return

