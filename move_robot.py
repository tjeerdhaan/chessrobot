import math
from robot_kinematics import inverse_kinematics

class Move(object):
    def __init__(self, sensor_state, controller1, controller2, motor1, motor2, frequency):
        self.sensor_state = sensor_state
        self.frequency = frequency
        self.controller1 = controller1
        self.controller2 = controller2
        self.motor1 = motor1
        self.motor2 = motor2
        self.calibrated1 = False
        self.calibrated2 = False
        self.calibrated = self.calibrated1 and self.calibrated2
        self.moving = False
        self.t_step = 0
        self.required_steps = 0
        self.at_home1 = False
        self.at_home2 = False
        self.at_home = self.at_home1 and self.at_home2
        return

    def home(self):
        homing_velocity = math.pi/8
        q1_home = 0
        if self.calibrated and not self.at_home:
            # This block is to check if the function is called for the first time, so that the reference position
            # from which the movement is started is saved.
            if self.moving == False:
                self.sensor_state.set_reference()
                self.required_steps = abs(q1_home - self.sensor_state.q1_reference)//(homing_velocity/self.frequency)
                self.t_step = 0
                self.moving = True

            # Divide the necessary movement into n segments of length v_home * t_step
            if self.t_step < self.required_steps:
                if q1_home - self.sensor_state.q1_reference < 0:
                    self.sensor_state.q1_reference -= homing_velocity/self.frequency 
                else:
                    self.sensor_state.q1_reference += homing_velocity/self.frequency
                control_value1 = self.controller1.step(self.sensor_state.q1_reference, self.sensor_state.joint1_angle)
                self.motor1.write(control_value1)
                self.t_step +=1
            # After n time steps, set reference to step response to actual home position
            elif self.t_step >= self.required_steps:
                self.sensor_state.q1_reference = q1_home
                if abs(self.sensor_state.joint1_angle) < 0.005:
                    self.at_home1 = True
                    self.motor1.write(0)
                else:
                    control_value1 = self.controller1.step(self.sensor_state.q1_reference, self.sensor_state.joint1_angle)
                    self.motor1.write(control_value1)
                    self.t_step += 1
            # As q2 home_position is same as calibration position, we will just reuse the calibration function for this part
            if self.sensor_state.endstop2_value == 0:
                self.motor2.write(0)
                self.sensor_state.joint2_angle = math.pi*156/180
                self.at_home2 = True
            else:
                self.motor2.write(0.6)
            
            # Check if at_home
            if self.at_home1 and self.at_home2:
                self.moving = False
                self.at_home = True
                self.t_step = 0
                print('Robot in home position')
        return
    
    def calibrate(self):
        if not self.calibrated2:
            if self.sensor_state.endstop2_value == 0:
                self.calibrated2 = True
                self.motor2.write(0)
                self.sensor_state.joint2_angle = math.pi*156/180
                self.sensor_state.encoder2.set_counter(0)
            else:
                self.motor2.write(0.6)
        elif not self.calibrated1:
            if self.sensor_state.endstop1_value == 0:
                self.calibrated1 = True
                self.calibrated = True
                self.motor1.write(0)
                self.sensor_state.joint1_angle = -math.pi/2
                self.sensor_state.encoder1.set_counter(0)
                self.sensor_state.set_reference()
                self.t_step = 0
            else:
                value = 0.65 -self.t_step*0.0002
                if value <= 0.58:
                    value = 0.58
                self.motor1.write(-value)
                self.t_step+=1
        return

    def move(self, velocity):
        flat = velocity.flatten()
        if flat[0] == 0 and flat[1] == 0:
            self.motor1.write(0)
            self.motor2.write(0)
            self.moving = False
        else:
            if not self.moving:
                self.sensor_state.set_reference()
                self.moving = True
            joint_angles = [self.sensor_state.joint1_angle, self.sensor_state.joint2_angle]
            dq = inverse_kinematics(velocity, joint_angles).flatten()
            self.sensor_state.q1_reference += dq[0]/self.frequency
            self.sensor_state.q2_reference += dq[1]/self.frequency
            control1 = self.controller1.step(self.sensor_state.q1_reference, self.sensor_state.joint1_angle)
            control2 = self.controller2.step(self.sensor_state.q2_reference, self.sensor_state.joint2_angle)
            self.motor1.write(control1)
            self.motor2.write(control2)
        return





