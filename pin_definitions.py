

class Pins(object):

    MOTOR_1_DIRECTION = 'D4'
    MOTOR_1_PWM = 'D5'
    POT_METER = 'A5'
    MOTOR_2_DIRECTION = 'D7'
    MOTOR_2_PWM = 'D6'
    MOTOR_1_Encoders = ['D0', 'D1']
    MOTOR_2_Encoders = ['D11', 'D12']
    MOTOR_2_Endstop = 'D3'
    MOTOR_1_Endstop = 'D2'
    EMG_PINS = ['A3','A2','A1', 'A0']
    SERVO_LINEAR_ACTUATOR = 'D36'
    SERVO_GRIPPER = 'D35'
    SHIELD_BUTTON_1 = 'D42' #PE_8
    SHIELD_BUTTON_2 = 'D37' #PE_15

