try:
    import ulab as np
    from ulab import linalg
except:
    import numpy as np

def tilde(T):
    # return Tilde form of a vector-twist
    Tt = np.zeros((3,3))
    Tt[0,1] = -T[0]
    Tt[1,0] = T[0]
    Tt[:2,2] = T[1:]
    
    return Tt

def adjoint(H):
    AdH = np.zeros((3,3))
    AdH[0,0] = 1
    AdH[1:,1:] = H[:2,:2]
    AdH[1,0] = H[1,2]
    AdH[2,0] = -H[0,2]
    
    return AdH

def inverseH(H):
    Hinv = np.eye(3)
    Hinv[:2,:2] = H[:2,:2].transpose()
    Hinv[:2,2] = linalg.dot(-Hinv[:2,:2], H[:2,2])
    return Hinv