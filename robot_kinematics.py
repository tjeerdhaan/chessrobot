from rki_operators import inverseH, adjoint
import math

try:
    import ulab as np
    from ulab import linalg
except ImportError:
    import numpy as np

q1 = 3*math.pi/4
q2 = math.pi/2
q = np.array([q1,q2])

def calculate_He(q1, q2):
    """
    Calculates the location of the end effector given 
    the 2 joint angles in radians.

    OUTPUT: He_0 (3x3 array)

    INPUT: q1,q2 (float)

    """
    #initial H for brockets
    He_00 = np.array([[1, 0, 1],[0, 1, 0],[0, 0 ,1]])

    #definining matrix exponentials for brockets
    T1_exp = np.array([[math.cos(q1), -math.sin(q1), 0], 
                    [math.sin(q1), math.cos(q1), 0], 
                    [0 ,0, 1]])

    T2_exp = np.array([[math.cos(q2), -math.sin(q2), 0.5 - 0.5*math.cos(q2)],
                    [math.sin(q2), math.cos(q2), -0.5*math.sin(q2)],
                    [0, 0, 1]])

    #forward kinematics using brockets
    He_0 = linalg.dot(T1_exp, linalg.dot(T2_exp, He_00))
    return He_0

def calculate_velocity(q, dq):
    """
    Calculates the point-wise velocity of the end effector expressed in the reference 
    frame.

    OUTPUT: velocity (1x2 array)

    INPUT: Joint angles q ([2,] array), joint velocities (1x2 array)
    """
    #Jacobian for forwards differential kinematics
    J = np.array([[1,0,0], [1, 0.5*math.sin(q[0]), -0.5*math.cos(q[0])]]).transpose()
    Te_0 = linalg.dot(J, dq)
    He_0 = calculate_He(q[0], q[1])
    Hf_0 = np.eye(3)
    Hf_0[:, 2] = He_0[:, 2]
    Te_0_f = linalg.dot(adjoint(inverseH(Hf_0)), Te_0)
    return Te_0_f[1:]

def inverse_kinematics(v_desired, q):
    """
    Calculate required joint velocities given a desired end effector velocity, using a pseudo-inverse of the jacobian.
    
    OUTPUT: joint velocity (2,) array

    INPUT: desired velocity (1x2 array)

    """
    He_0 = calculate_He(q[0], q[1])
    Hf_0 = np.eye(3)
    Hf_0[:, 2] = He_0[:, 2]
    adj = adjoint(inverseH(Hf_0)) 
    J = np.array([[1,0,0], [1, 0.5*math.sin(q[0]), -0.5*math.cos(q[0])]]).transpose()
    J_dagger = linalg.dot(adj, J)
    J_2x2 = np.zeros((2,2))
    J_2x2[0,:] = J_dagger[1,:]
    J_2x2[1,:] = J_dagger[2,:]
    J_inverse = np.linalg.inv(J_2x2)
    joint_velocity = linalg.dot(J_inverse, v_desired)
    return joint_velocity

