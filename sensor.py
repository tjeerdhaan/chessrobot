from biorobotics import AnalogIn, Encoder
from switch import BlueSwitch, Button
from pin_definitions import Pins
from pyb import Pin
import math

class SensorState(object):

    def __init__(self, frequency):
        self.blue_switch = BlueSwitch()
        self.switch_value = 0
        self.pot_meter = AnalogIn(Pins.POT_METER)
        self.pot_value = 0
        self.encoder1 = Encoder(Pins.MOTOR_1_Encoders[0], Pins.MOTOR_1_Encoders[1])
        self.encoder2 = Encoder(Pins.MOTOR_2_Encoders[0], Pins.MOTOR_2_Encoders[1])
        self.button1 = Button(Pins.SHIELD_BUTTON_1)
        self.button2 = Button(Pins.SHIELD_BUTTON_2)
        self.button1_value = 0
        self.button2_value = 1
        self.joint1_angle = 0
        self.joint2_angle = 0
        self.endstop1 = Pin(Pins.MOTOR_1_Endstop, Pin.IN, Pin.PULL_UP)
        self.endstop2 = Pin(Pins.MOTOR_2_Endstop, Pin.IN, Pin.PULL_UP)
        self.joint1_previous = [0, 0]
        self.joint2_previous = [0, 0]
        self.q1_reference = -math.pi/2
        self.q2_reference = math.pi/180*156
        self.frequency = frequency
        self.emgs = [AnalogIn(Pins.EMG_PINS[0]), AnalogIn(Pins.EMG_PINS[1]), AnalogIn(Pins.EMG_PINS[2]), AnalogIn(Pins.EMG_PINS[3])]
        return
    
    def update(self):
        self.switch_value = self.blue_switch.value()
        self.pot_value = self.pot_meter.read()
        self.encoder1_counts = self.encoder1.counter()
        self.encoder2_counts = self.encoder2.counter()
        self.endstop2_value = self.endstop2.value()
        self.endstop1_value = self.endstop1.value()

        #button values are 1 if not pressed, 0 if pressed
        self.button1_value = self.button1.value()
        self.button2_value = self.button2.value()
        
        self.track_joint_angles()
        self.calculate_joint_velocity()
        index = 0
        self.emg_values = [0, 0, 0, 0]
        for emg in self.emgs:
            self.emg_values[index] = emg.read()
            index += 1
        return

    def track_joint_angles(self):
        """
        Gives joint angle in radians.
        After calibration, this function tracks the angles of the joints, 
        used for the robot kinematics by using the encoder count of the motors
        """
        self.joint1_angle = -math.pi/2 + math.pi/4200*self.encoder1_counts
        self.joint2_angle = math.pi/180*156 - math.pi/4200*self.encoder2_counts
        return

    def calculate_joint_velocity(self):
        """
        Calculates joint velocity in rad/sec
        """
        self.joint1_velocity = (self.encoder1_counts - self.joint1_previous[1])*self.frequency*math.pi/4200/2
        self.joint2_velocity = (self.encoder2_counts - self.joint2_previous[1])*self.frequency*math.pi/4200/2
        self.joint1_previous[1] = self.joint1_previous[0]
        self.joint1_previous[0] = self.encoder1_counts
        self.joint2_previous[1] = self.joint2_previous[0]
        self.joint2_previous[0] = self.encoder2_counts
        return

    def set_reference(self):
        self.q1_reference = self.joint1_angle
        self.q2_reference = self.joint2_angle
        return