import time
from biorobotics import PWM
from pin_definitions import Pins

class Servo(object):

    def __init__(self, ticker):
        self.pwm_linear_actuator = PWM(Pins.SERVO_LINEAR_ACTUATOR, 50)
        self.pwm_gripper = PWM(Pins.SERVO_GRIPPER, 50)
        self.ticker = ticker
        self.direccion = True
        self.gripper_state = True
        return

    def up_down(self, turns = 1.2, degrees = 0.05):
        ''' Controlling the linear actuator up and down movement:

            turns: amount of full turns (1 equals one turn,
             2 equals two)). 
            
            degrees: values between 0.05 and 0.13 allow to turn 0 to 180 degrees extra
            
            direccion: True turns counterclockwise (down) and False turns 
            clockwise (up) 
        '''
        #Set initial position for accurate movement
        self.ticker.stop()
        turns = turns - 0.2
        self.pwm_linear_actuator.write(0.05)
        time.sleep(1) 
        self.pwm_linear_actuator.write(0.0)

        #Turn linear actuator down 
        if self.direccion == True:
            self.pwm_linear_actuator.write(0.7)
            time.sleep(turns)
            self.pwm_linear_actuator.write(degrees)
            time.sleep(1)
            self.pwm_linear_actuator.write(0.0)
            time.sleep(1) 
            self.direccion = False
            self.ticker.start()
            return

        #Turn linear actuator up
        elif self.direccion == False:
            self.pwm_linear_actuator.write(0.5)
            time.sleep(turns)
            self.pwm_linear_actuator.write(0.05)
            time.sleep(1)
            self.pwm_linear_actuator.write(0.0)
            time.sleep(1)
            self.direccion = True
            self.ticker.start()
            return

    def open_close(self):
        """
        Change the state of the gripper from open to close and from close
        to open return the current state for next accion 

        gripper_state: is a boolean 
                        True equals open 
                        False equals closed
        """
        #Close the gripper
        self.ticker.stop()
        if self.gripper_state == True:
            self.pwm_gripper.write(0.05)
            time.sleep(1.5)
            self.pwm_gripper.write(0.0)
            time.sleep(1)
            self.gripper_state = False
            self.ticker.start()
            return

        #Open the gripper
        else:
            self.pwm_gripper.write(0.13)
            time.sleep(1)
            self.pwm_gripper.write(0.0)
            time.sleep(1)
            self.gripper_state = True
            self.ticker.start()
            return