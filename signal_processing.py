from filter import Biquad

class Envelope(object):
    
    def __init__(self):

        #low_pass second order butterworth filter with w_c = 2 Hz, f_s 250Hz
        self.low_pass = Biquad([1.0000, -1.9289, 0.9314],[0.0006, 0.0012, 0.0006])

        #high_pass second order butterworth filter with w_c = 1 Hz, f_s 250Hz
        self.high_pass = Biquad([1.0000, -1.9645, 0.9651], [0.9824, -1.9648, 0.9824])
        
        self.max_value = 0
        return

    def envelope(self, sample):
        #apply high pass filter
        high_filtered = self.high_pass.step(sample)
        #rectify the signal
        rectified = abs(high_filtered)
        #apply low pass filter to create envelope
        self.emg_value = self.low_pass.step(rectified)/self.max_value
        return self.emg_value

    def calibrate_max(self, sample):
        #apply high pass filter
        high_filtered = self.high_pass.step(sample)
        #rectify the signal
        rectified = abs(high_filtered)
        #apply low pass filter to create envelope
        emg_value = self.low_pass.step(rectified)
        if emg_value > self.max_value:
            self.max_value = emg_value
        return