from states import State
from leds import Leds
from motor import Motor
from controller import PID
from biorobotics import SerialPC
import ulab as np
from signal_processing import Envelope
from move_robot import Move
from emg_control import Converter
from servos import Servo

class StateFunctions(object):

    def __init__(self, robot_state, sensor_state, pwm_frequency, ticker):

        self.robot_state = robot_state
        self.motor1 = Motor(1, 5000)
        self.motor2 = Motor(2, 5000)
        self.pc = SerialPC(2)
        self.sensor_state = sensor_state
        self.leds = Leds()
        self.controller1 = PID(120, 0, 15, 1/pwm_frequency)
        self.controller2 = PID(120, 0, 15, 1/pwm_frequency)
        self.ticker = ticker
        self.frequency = pwm_frequency
        self.in_reference_pos = False
        self.time_steps = 0
        self.envelope_bicep1 = Envelope()
        self.envelope_tricep1 = Envelope()
        self.envelope_bicep2 = Envelope()
        self.envelope_tricep2 = Envelope()
        self.move = Move(self.sensor_state, self.controller1, self.controller2, self.motor1, self.motor2, self.frequency)
        self.converter = Converter(self.envelope_bicep1, self.envelope_tricep1, self.envelope_bicep2, self.envelope_tricep2, self.sensor_state)
        self.zero = np.array([0,0]).transpose()
        self.servo = Servo(ticker)

        self.callbacks = {
            State.SAFE: self.safe,
            State.HOME: self.home,
            State.ON: self.on, 
            State.END_EFFECTOR: self.end_effector
        }
        return

    def safe(self):

        #Entry action
        if self.robot_state.is_changed():
            self.robot_state.set(self.robot_state.current)
            self.leds.green.on()

        self.motor1.write(0)
        self.motor2.write(0)

        #calibrating emg_values
        self.envelope_bicep1.calibrate_max(self.sensor_state.emg_values[0])
        self.envelope_tricep1.calibrate_max(self.sensor_state.emg_values[1])
        self.envelope_bicep2.calibrate_max(self.sensor_state.emg_values[2])
        self.envelope_tricep2.calibrate_max(self.sensor_state.emg_values[3])
        
        #State guards and exit action
        if self.sensor_state.switch_value == 1 or self.sensor_state.button1_value == 0 or self.sensor_state.button2_value == 0:
            self.robot_state.set(State.HOME)
            self.leds.green.off()  
        return
    
    def home(self):

        #Entry action
        if self.robot_state.is_changed():
            self.robot_state.set(self.robot_state.current)
            self.leds.yellow.on()

            #These were pre-determined emg calibration values, to ensure stability during the project presentation
            self.envelope_bicep1.max_value = 0.17678
            self.envelope_bicep2.max_value = 0.19678
            self.envelope_tricep1.max_value = 0.16170 
            self.envelope_tricep2.max_value = 0.16704

            self.move.t_step = 0
            self.move.moving = False

        #calibrate the position of the robot
        if not self.move.calibrated:
            self.move.calibrate()

        if not self.move.at_home:
            self.move.home()
        elif self.move.at_home:
            #check if still at home
            if abs(self.sensor_state.joint1_angle) > 0.005:
                self.move.at_home = False
                self.move.at_home1 = False
            if self.sensor_state.endstop2_value == 1:
                self.move.at_home = False
                self.move.at_home2 = False
        
        self.converter.emg_to_velocity(0.1)
        self.pc.set(0, self.envelope_bicep1.emg_value)
        self.pc.set(1, self.sensor_state.emg_values[0])        
        self.pc.send()


        #State guards and exit action
        if self.sensor_state.button1_value == 0:
            self.robot_state.set(State.ON)
            self.leds.yellow.off()
            self.time_steps = 0
            self.move.at_home = False
            self.move.at_home1 = False
            self.move.at_home2 = False
        elif self.sensor_state.button2_value == 0:
            self.robot_state.set(State.SAFE)
            self.leds.yellow.off()
            self.time_steps = 0

        return

    def on(self):

        #Entry action
        if self.robot_state.is_changed():
            self.robot_state.set(self.robot_state.current)
            self.leds.red.on()
            self.time_steps = 0
            self.sensor_state.set_reference()
            self.move.at_home = False
            self.move.at_home1 = False
            self.move.at_home2 = False
        
        v_max = 0.25
        velocity = self.converter.emg_to_velocity(v_max)
        self.pc.set(0, self.converter.arm1)
        self.pc.set(1, self.converter.arm2)
        self.pc.send()

        #Creates threshold of 10% of maximum muscle contraction for movement to happen
        if abs(self.converter.arm1) > 0.1 or abs(self.converter.arm2) > 0.1:
            self.move.move(velocity)
        else:
            #Ensures robot remains in position if disturbed.
            control_value1 = self.controller1.step(self.sensor_state.q1_reference, self.sensor_state.joint1_angle)
            control_value2 = self.controller2.step(self.sensor_state.q2_reference, self.sensor_state.joint2_angle)
            self.motor1.write(control_value1)
            self.motor2.write(control_value2)
        
        #State guards and exit action
        if self.sensor_state.switch_value == 1:
            self.robot_state.set(State.END_EFFECTOR)
            self.leds.red.off()
            self.motor1.write(0)
            self.motor2.write(0)
        elif self.sensor_state.button1_value == 0:
            self.robot_state.set(State.END_EFFECTOR)
            self.motor1.write(0)
            self.motor2.write(0)
            self.leds.red.off()
        elif self.sensor_state.button2_value == 0:
            self.robot_state.set(State.HOME)
            self.motor1.write(0)
            self.motor2.write(0)
            self.leds.red.off()
        return

    def end_effector(self):

        #Entry action
        if self.robot_state.is_changed():
            self.robot_state.set(self.robot_state.current)
            self.leds.yellow.on()
            self.leds.green.on()
            self.motor1.write(0)
            self.motor2.write(0)
            self.time_steps = 0
            self.triggered = False

        self.envelope_bicep1.envelope(self.sensor_state.emg_values[0])
        self.envelope_bicep2.envelope(self.sensor_state.emg_values[2])
        self.pc.set(0, self.envelope_bicep1.emg_value)
        self.pc.set(1, self.envelope_bicep2.emg_value)
        self.pc.send()

        control_value1 = self.controller1.step(self.sensor_state.q1_reference, self.sensor_state.joint1_angle)
        control_value2 = self.controller2.step(self.sensor_state.q2_reference, self.sensor_state.joint2_angle)
        self.motor1.write(control_value1)
        self.motor2.write(control_value2)
        
        if not self.triggered:
            if self.envelope_bicep1.emg_value > 0.3:
                # Move linear actuator up/down
                self.servo.up_down(1, 0.13)
                self.time_steps = 0
                self.triggered = True
            elif self.envelope_bicep2.emg_value > 0.3:
                # Open/close Gripper 
                self.servo.open_close()
                self.triggered = True
                self.time_steps = 0
        else:
            if self.time_steps > 75:
                self.triggered = False
            else:
                self.time_steps += 1
    
        #State guards and exit action
        if self.sensor_state.switch_value == 1:
            self.robot_state.set(State.ON)
            self.leds.yellow.off()
            self.leds.green.off()
        elif self.sensor_state.button1_value == 0:
            self.robot_state.set(State.ON)
            self.leds.yellow.off()
            self.leds.green.off()
        elif self.sensor_state.button2_value == 0:
            self.robot_state.set(State.HOME)
            self.leds.yellow.off()
            self.leds.green.off()
        return
    
    
