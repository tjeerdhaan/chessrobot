from biorobotics import Ticker
from state_functions import StateFunctions
from states import State
from sensor import SensorState

class StateMachine(object):

    def __init__(self, ticker_frequency):
        self.robot_state = State()
        self.ticker = Ticker(0, ticker_frequency, self.run)
        self.sensor_state = SensorState(ticker_frequency)
        self.state_functions = StateFunctions(self.robot_state, self.sensor_state, ticker_frequency, self.ticker)
        return

    def run(self):
        self.sensor_state.update()
        self.state_functions.callbacks[self.robot_state.current]()
        return

    def start(self):
        self.ticker.start()
        return
    
    def stop(self):
        self.ticker.stop()
        return
