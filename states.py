class State(object):
    SAFE = 'safe'
    HOME = 'home'
    ON = 'on'
    END_EFFECTOR = 'end_effector'

    def __init__(self):
        self.current = self.SAFE
        self.previous = None
        return

    def set(self, state):
        self.previous = self.current
        self.current = state
        return

    def is_changed(self):
        return self.previous is not self.current
