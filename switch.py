from pyb import Switch, Pin
import biorobotics
import micropython
from pin_definitions import Pins

class BlueSwitch(object):

    def __init__(self):
        self.switch_value = 0
        self.switch = Switch()
        self.switch.callback(self.callback)
        biorobotics.tic()
        return

    def callback(self):
        micropython.heap_unlock()
        time_since_press = biorobotics.toc()
        biorobotics.tic()
        if time_since_press > 1000:
            self.switch_value = 1

        micropython.heap_lock()
        return

    def value(self):
        return_value = self.switch_value
        self.switch_value = 0
        return return_value

class Button(object):

    def __init__(self, pin):
        self.button_value = 1
        self.button = Pin(pin, Pin.IN, Pin.PULL_UP)
        biorobotics.tic()
        return
    
    def value(self):
        # Debounces button, button press can only be registered every 0.3 seconds
        if self.button.value() == 0:
            time_since_press = biorobotics.toc()
            if time_since_press < 300000:
                self.button_value = 1
            else:
                self.button_value = 0
                biorobotics.tic()
        else:
            self.button_value = 1
        return self.button_value



    